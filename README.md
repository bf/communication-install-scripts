These are scripts to install communication tools on Trisquel:

 * BigBlueButton is a conference system that allows to have N-to-N conversations sharing camera feeds, screencasts, slides, etherpad and chat.
 * Canvas is a Learning Management System, it integrates with BBB

## TO DO:

 * Improve license selector for uploaded content on Canvas, removing nonfree options and adding GPL and FDL
 * Remove integration with non-free services/products
 * Add script for Redmine
